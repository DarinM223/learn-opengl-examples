#version 330 core

out vec4 color;

uniform vec4 ourColor; // Set in OpenGL code.

void main()
{
    color = ourColor;
}
