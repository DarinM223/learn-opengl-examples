#ifndef GAME_H
#define GAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <array>
#include <exception>
#include <glm/glm.hpp>

class GlfwWindowException : public std::exception {
 public:
  const char *what() const noexcept override {
    return "Failed to create GLFW window";
  }
};

class GlewInitException : public std::exception {
 public:
  const char *what() const noexcept override {
    return "Failed to initialize GLEW";
  }
};

class Game {
 public:
  Game();
  void run();
  ~Game();

 protected:
  virtual void processInput();
  virtual void moveCamera();

 private:
  friend void keyCallback(GLFWwindow *, int, int, int, int);
  friend void mouseCallback(GLFWwindow *, double, double);
  friend void scrollCallback(GLFWwindow *, double, double);

  GLFWwindow *window;
  int width;
  int height;
  std::array<bool, 1024> keys{};

  // Mouse variables
  int lastX{0};
  int lastY{0};
  bool firstMouse{true};

  // Camera variables
  GLfloat yaw{-90.0f};
  GLfloat pitch{0.0f};
  GLfloat fov{45.0f};
  glm::vec3 cameraPos{0.0f, 0.0f, 3.0f};
  glm::vec3 cameraFront{0.0f, 0.0f, -1.0f};
  glm::vec3 cameraUp{0.0f, 1.0f, 0.0f};
};

#endif
