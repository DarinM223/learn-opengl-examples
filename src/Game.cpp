#include "Game.h"
#include <array>
#include <cmath>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Image.h"
#include "Shader.h"

void keyCallback(GLFWwindow *window, int key, int scanCode, int action,
                 int mods) {
  Game *game = reinterpret_cast<Game *>(glfwGetWindowUserPointer(window));
  if (action == GLFW_PRESS) {
    game->keys[key] = true;
  } else if (action == GLFW_RELEASE) {
    game->keys[key] = false;
  }
}

void mouseCallback(GLFWwindow *window, double xpos, double ypos) {
  Game *game = reinterpret_cast<Game *>(glfwGetWindowUserPointer(window));

  if (game->firstMouse) {
    game->lastX = xpos;
    game->lastY = ypos;
    game->firstMouse = false;
  }

  GLfloat xOffset = xpos - game->lastX;
  GLfloat yOffset = ypos - game->lastY;
  game->lastX = xpos;
  game->lastY = ypos;

  GLfloat sensitivity = 0.05f;
  xOffset *= sensitivity;
  yOffset *= sensitivity;

  game->yaw += xOffset;
  game->pitch += yOffset;

  if (game->pitch > 89.0f) {
    game->pitch = 89.0f;
  }
  if (game->pitch < -89.0f) {
    game->pitch = -89.0f;
  }

  glm::vec3 front{
      std::cos(glm::radians(game->yaw)) * std::cos(glm::radians(game->pitch)),
      std::sin(glm::radians(game->pitch)),
      std::sin(glm::radians(game->yaw)) * std::cos(glm::radians(game->pitch))};
  game->cameraFront = glm::normalize(front);
}

void scrollCallback(GLFWwindow *window, double xOffset, double yOffset) {
  Game *game = reinterpret_cast<Game *>(glfwGetWindowUserPointer(window));

  if (game->fov >= 1.0f && game->fov <= 45.0f) {
    game->fov -= yOffset;
  }
  if (game->fov <= 1.0f) {
    game->fov = 1.0f;
  }
  if (game->fov >= 45.0f) {
    game->fov = 45.0f;
  }
}

Game::Game() {
  glfwInit();
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  this->window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
  if (this->window == nullptr) {
    throw GlfwWindowException{};
  }

  glfwMakeContextCurrent(this->window);
  glfwGetFramebufferSize(this->window, &this->width, &this->height);
  glViewport(0, 0, this->width, this->height);

  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    throw GlewInitException{};
  }

  glEnable(GL_DEPTH_TEST);
  glfwSetWindowUserPointer(this->window, this);
  glfwSetKeyCallback(this->window, keyCallback);
  glfwSetCursorPosCallback(this->window, mouseCallback);
  glfwSetScrollCallback(this->window, scrollCallback);
}

void Game::run() {
  Shader shader{"./shaders/coordinate.vs", "./shaders/coordinate.frag"};

  // Load textures.
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  {
    Image image{"./assets/container.jpg"};
    if (image.get() == nullptr) {
      throw ImageLoadException{};
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.width(), image.height(), 0,
                 GL_RGB, GL_UNSIGNED_BYTE, image.get());
    glGenerateMipmap(GL_TEXTURE_2D);
  }
  glBindTexture(GL_TEXTURE_2D, 0);

  // clang-format off
  GLfloat vertices[] = {
      -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
       0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
      -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

      -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
      -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
       0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
       0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
       0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
       0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
       0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
      -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
      -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
  };
  // clang-format on

  std::array<glm::vec3, 10> cubePositions{
      glm::vec3{0.0f, 0.0f, 0.0f},    glm::vec3{2.0f, 5.0f, -15.0f},
      glm::vec3{-1.5f, -2.2f, -2.5f}, glm::vec3{-3.8f, -2.0f, -12.3f},
      glm::vec3{2.4f, -0.4f, -3.5f},  glm::vec3{-1.7f, 3.0f, -7.5f},
      glm::vec3{1.3f, -2.0f, -2.5f},  glm::vec3{1.5f, 2.0f, -2.5f},
      glm::vec3{1.5f, 0.2f, -1.5f},   glm::vec3{-1.3f, 1.0f, -1.5f}};

  // Generate a vertex buffer object.
  GLuint VAO, VBO, EBO;
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                        (GLvoid *)0);
  glEnableVertexAttribArray(0);

  // texture coordinate attribute
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat),
                        (GLvoid *)(3 * sizeof(GLfloat)));
  glEnableVertexAttribArray(2);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  // Uncomment to show in wireframe.
  // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  while (!glfwWindowShouldClose(this->window)) {
    glfwPollEvents();
    this->processInput();
    this->moveCamera();

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Bind texture and set uniform.
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(glGetUniformLocation(shader.program(), "ourTexture"), 0);

    shader.use();

    // Transformations that apply to all vertices in an object.
    glm::mat4 model{};
    model = glm::rotate(model, glm::radians((GLfloat)glfwGetTime() * 50.0f),
                        glm::vec3{1.0f, 0.0f, 0.0f});

    // Transformations that apply to the view (camera).
    glm::mat4 view{};
    view = glm::lookAt(this->cameraPos, this->cameraPos + this->cameraFront,
                       this->cameraUp);

    // The perspective projection matrix.
    glm::mat4 projection{};
    projection =
        glm::perspective(glm::radians(this->fov),
                         (GLfloat)this->width / this->height, 0.1f, 100.0f);

    GLuint modelLoc = glGetUniformLocation(shader.program(), "model");

    GLuint viewLoc = glGetUniformLocation(shader.program(), "view");
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

    GLuint projectionLoc = glGetUniformLocation(shader.program(), "projection");
    glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

    glBindVertexArray(VAO);
    for (int i = 0; i < cubePositions.size(); i++) {
      glm::mat4 model{};
      model = glm::translate(model, cubePositions[i]);
      model = glm::rotate(model, glm::radians(20.0f * i),
                          glm::vec3{1.0f, 0.3f, 0.5f});
      glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    glfwSwapBuffers(this->window);
  }
}

void Game::processInput() {
  if (glfwGetKey(this->window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, true);
  }
}

void Game::moveCamera() {
  GLfloat cameraSpeed = 0.01f;
  if (this->keys[GLFW_KEY_W]) {
    this->cameraPos += cameraSpeed * this->cameraFront;
  }
  if (this->keys[GLFW_KEY_S]) {
    this->cameraPos -= cameraSpeed * this->cameraFront;
  }
  if (this->keys[GLFW_KEY_A]) {
    this->cameraPos -=
        glm::normalize(glm::cross(this->cameraFront, this->cameraUp)) *
        cameraSpeed;
  }
  if (this->keys[GLFW_KEY_D]) {
    this->cameraPos +=
        glm::normalize(glm::cross(this->cameraFront, this->cameraUp)) *
        cameraSpeed;
  }
}

Game::~Game() { glfwTerminate(); }
